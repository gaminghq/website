ARG COMPOSER_VERSION=1.9
FROM composer:${COMPOSER_VERSION}

# Install ext-intl
RUN apk add --no-cache icu-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

CMD ["composer", "install", "--ignore-platform-reqs", "--no-scripts"]
