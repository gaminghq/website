<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
  $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
  $routes->connect('/ban-appeals', ['controller' => 'Pages', 'action' => 'display', 'ban-appeal']);
  $routes->connect('/discord', ['controller' => 'Pages', 'action' => 'display', 'discord']);
  $routes->connect('/dmca', ['controller' => 'Pages', 'action' => 'display', 'dmca']);
  $routes->connect('/our-servers', ['controller' => 'Pages', 'action' => 'display', 'our-servers']);
  $routes->connect('/partner', ['controller' => 'Pages', 'action' => 'display', 'partner']);
  $routes->connect('/privacy', ['controller' => 'Pages', 'action' => 'display', 'privacy']);
  $routes->connect('/support', ['controller' => 'Pages', 'action' => 'display', 'support']);

  /**
   * Add scope for our gameservers
   */
  $routes->scope('/servers', function(RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Servers', 'action' => 'index']);
    $routes->connect('/:server', ['controller' => 'Servers', 'action' => 'display'])
      ->setPass(['server']);
  });

  /**
   * Add scope for our news
   */
  $routes->scope('/news', function(RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Blog', 'action' => 'index']);
    $routes->connect('/:slug', ['controller' => 'Blog', 'action' => 'view'])
      ->setPass(['slug']);
  });

  $routes->fallbacks(DashedRoute::class);
});
