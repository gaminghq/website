<?php declare(strict_types=1);
  namespace App\Controller;

  use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
  use Cake\Http\Exception\NotFoundException;
  use Cake\View\Exception\MissingTemplateException;

  class ServersController extends AppController {
    public function beforeFilter(EventInterface $event): void {
      $this->Auth->allow(['index', 'display']);
    }

    public function index() {}

    public function display(string ...$game) {
      // Change our layout template
      $this->viewBuilder()->setLayout('game');

      // Make sure a game is defined
      if (!$game) return $this->redirect('/');

      // Prevent directory traversal
      if (in_array('..', $game, true) || in_array('.', $game, true)) throw new ForbiddenException();

      // Try rendering the page
      try {
        return $this->render(implode('/', $game));
      } catch (MissingTemplateException $exception) {
        // Check if we are debugging
        // If so, throw our MissingTemplateException
        // Else, throw a NotFoundException
        if (Configure::read('debug')) throw $exception;
        throw new NotFoundException();
      }
    }
  }
