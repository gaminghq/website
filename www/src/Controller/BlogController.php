<?php declare(strict_types=1);
  namespace App\Controller;

  use App\Controller\AppController;
  use Cake\Event\EventInterface;
  use Cake\Http\Exception\NotFoundException;

  class BlogController extends AppController {
    public function initialize(): void {
      parent::initialize();

      // Load required models
      $this->loadModel('Admiral/Blog.Articles');
    }

    public function beforeFilter(EventInterface $event): void {
      $this->Auth->allow(['index', 'view']);
    }

    public function index() {
      // Get our public posts
      $articles = $this->Articles
        ->find()
        ->where(['published' => 1])
        ->order([
          'created' => 'DESC',
          'modified' => 'DESC',
        ])
        ->all();
        
      // Set our viewvars
      $this->set(compact('articles'));
    }

    public function view(string $slug = null) {
      $article = $this->Articles
        ->find()
        ->where([
          'published' => 1,
          'slug' => $slug,
        ])
        ->first();
      
      if(!$article) throw new NotFoundException('This article could not be found');

      $this->set(compact('article'));
    }
  }
