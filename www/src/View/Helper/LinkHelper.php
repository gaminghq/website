<?php declare(strict_types=1);
  namespace App\View\Helper;

  Use Cake\View\Helper;

  class LinkHelper extends Helper {
    public function isSteam(string $url): bool {
      return parse_url($url, PHP_URL_SCHEME) === 'steam';
    }

    public function gameUrl(string $game): string {
      $game = str_replace(' ', '-', $game);
      $game = str_replace('/', '-', $game);
      $game = strtolower($game);
      return $game;
    }
  }
