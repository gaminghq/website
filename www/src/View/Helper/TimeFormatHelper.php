<?php declare(strict_types=1);
  namespace App\View\Helper;

  Use Cake\View\Helper;

  class TimeFormatHelper extends Helper {

    /**
     * Returns a formal date string like "Feb 28, 2021 at 11:46 (UTC)"
     * 
     * @param \Cake\I18n\Time|\Cake\I18n\FrozenTime $date The input date
     * @return string The formatted date
     */
    public function formal($date): string {
      // Create a variable to hold our output
      $output = '';

      // Get the month, day and year
      $output .= $date->i18nFormat('MMM dd, yyyy');

      // Get the time
      $output .= ' at ';
      $output .= $date->i18nFormat('HH:mm');

      // Get the timezone
      $output .= ' (';
      $output .= $date->format('e');
      $output .= ')';

      // Return the result
      return $output;
    }

    public function short($date): string {
      return $date->i18nFormat('MMM dd, yyyy');
    }
  }
