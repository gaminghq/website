<?php declare(strict_types=1);
  namespace App\View\Helper;

  use Cake\View\Helper;
  use Cake\Filesystem\File;

  class ImageHelper extends Helper {
    /**
     * Turn an image into a base64 encoded string.
     * 
     * @param string $image The path to the image.
     * @throws \Exception
     * @return string The base64 encoded url
     */
    public function asString(string $image): string {
      // Load the file
      $file = new File(WWW_ROOT . $image);
      
      // Make sure it exists
      if(!$file->exists()) throw new \Exception('Image does not exist!');

      // Read the file
      // Then convert it to base64
      $bytes = $file->read(true);
      $encoded = \base64_encode($bytes);

      return 'data:image/' . $file->ext() . ';base64,' . $encoded;
    }
  }