<?php
// src/Shell/InstallShell.php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class InstallShell extends Shell {
  public function migrations() {
    // Load a list of the plugins
    $plugins = Plugin::loaded();

    // Define the order in which to migrate the plugins
    $order = [
      'Admiral/Admiral',
      'Admiral/Blog',
    ];

    // exclude plugins
    $excludes = [
      'DebugKit',
      'Bake',
      'Migrations',
      'WyriHaximus/TwigView'
    ];

    foreach($excludes as $exclude) {
      // Remove the plugin from the list
      unset($plugins[array_search($exclude, $plugins)]);
    }

    // Loop over all the plugins in the order list
    foreach($order as $entry){
      // Check if the plugin is installed
      if(in_array($entry,$plugins)){
        // Plugin is installed
        // Run it's migrations
        $this->dispatchShell('migrations','migrate','-p',$entry);
      }

      // Remove the plugin from the list
      unset($plugins[array_search($entry, $plugins)]);
    }

    // Migrate the left-over plugins
    foreach($plugins as $plugin) {
      $this->dispatchShell('migrations','migrate','-p',$plugin);
    }

    // Migrate the main app
    $this->dispatchShell('migrations','migrate');
  }
}