<?= $this->element('layout/game-menu', [
  'items' => [
    'Server Information' => '#1info',
    'Why Radsz?' => '#2info',
    'Rules' => '#serverrules',
  ]
]); ?>

<!-- Header -->
<div class="nk-main">
  <div class="nk-header-title nk-header-title-full nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-4"><img src="/img/image-3-rust-3.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <div class="nk-gap-2"></div>
            <h1 class="nk-title display-4">Radsz - PvEvP Roleplay </h1>
            <div class="nk-gap-2"></div>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://store.steampowered.com/app/252490/Rust/" class="nk-btn nk-btn-lg">
              <span class="icon ion-steam"></span>
              <span>Buy on Steam</span>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?= $gameList['Modded Rust']['server']['link'] ?? '#'; ?>" class="nk-btn nk-btn-lg">
              <span class="icon ion-steam"></span>
              <span>Join the server</span>
            </a>
            <div class="nk-gap-2"></div>
            <div class="nk-gap-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- About -->
  <div class="nk-box bg-dark-1" id="1info">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Radsz - The PvEvP Roleplay server</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        The world of Radsz awaits you.
        While you wake up slowly, you find out that you got dumped on a island with nothing more then a simple rock and a torch.
        The last thing you remember is the strange interview you had with the cobalt test facility.
        While you walk the world of Radsz, you find out that you are not alone.
        Wildlife, loyal ex scientists and other humans just like you dumped in the middle of nowhere.
      </p>
      <p class="lead">
        While you are trying to search for answers, the world works against you.
        Hostiles, wildlife and other players are trying to do what it takes to survive.
        Are you ready to enter the world of radsz and are you going to be the next hero that everyone is waiting for?
      </p>
      <div class="nk-gap-6"></div>
    </div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1" id="2info">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img1.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title">Prep the Base and start to survive!</h2>
          <h3 class="nk-sub-title text-white">Start gathering your Resources and build up your base.</h3>
          <h3 class="nk-sub-title text-white">Gather food, hunt and scavange for materials to stay alive.</h3>
          <h3 class="nk-sub-title text-white">Watch out for wildlife, Patrolling AI or even worse, hostile players.</h3>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img6.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Custom mapping</h2>
          <h3 class="nk-sub-title text-white">With over several awesome custom made puzzles, zones and highly detailed.</h3>
          <h2 class="nk-sub-title text-white">Custom tunnels, caves and hidden secrets to be found during your adventure.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img2.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Custom events</h2>
          <h3 class="nk-sub-title text-white">Custom events from time to time. Danger is always closer then you think.</h3>
          <h2 class="nk-sub-title text-white">Custom bradley spawns and custom AI spawns across the world of Radsz.</h2>
          <h2 class="nk-sub-title text-white">With a angry patrol helicopter shooting anyone on sight.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img3.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Including</h2>
          <h3 class="nk-sub-title text-white">Many well known plugins like Furnance splitter, stack size controller and more! </h3>
          <h2 class="nk-sub-title text-white">With over 40+ challenges and a wicked leaderboard to challenge other players for their tags.</h2>
          <h2 class="nk-sub-title text-white">Lots and lots of funtime with friends and clanmembers.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img4.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Monthly Wipes</h2>
          <h3 class="nk-sub-title text-white">Every first Thursday of the month. </h3>
          <h2 class="nk-sub-title text-white">With Blueprints when needed.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box bg-dark-1" id="serverrules">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Gameserver rules</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        These rules you see here will be on most community servers.
        However, to keep everything clean and tidy, we have some rules setup in order to give you a good example on what's posible and what's not possible on our game server.
        By joining our server, you accept to abide by these rules.
      </p>
      <div class="nk-gap-2"></div>
      <ol>
        <li>You dont advertise on our server for your own benefits. skin gamble sites are a instant perm ban.</li>
        <li>Dont be a troll in our gameserver. Respect others their mentions.</li>
        <li>No hate speeches, political slures, racism or sexual chat sessions.</li>
        <li>No hate speeches, political slures, racism or sexual images.</li>
        <li>There is no such thing as raiding, check the ingame server information to know how we handle this part of the game with a roleplay server.</li>
        <li>The useage of any macro scripting, hacking or anything that needs a 3th party. Hacking is forbidden. Simple.</li>
        <li>Harrassing partners and or the team with unneeded questions and trashtalk is forbidden.</li>
        <li>Its forbidden to streamsnipe. People doing this will be banned.</li>
        <li>More rules can be found ingame on our information tab.</li>
      </ol>
      <div class="nk-gap-2"></div>
      <p>
        <a href="<?= $gameList['Modded Rust']['server']['link'] ?? '#'; ?>" target="_blank" class="nk-btn nk-btn-lg nk-btn-color-main-1 link-effect-4">
          <span>Join the server</span>
        </a>
      </p>
      <div class="nk-gap-6"></div>
    </div>
  </div>
</div>
