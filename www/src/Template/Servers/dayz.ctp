<?= $this->element('layout/game-menu', [
  'items' => [
    'Server Introduction' => '#1info',
    'Why Arkadia?' => '#2info',
    'Server Information' => '#info',
    'Media' => '#video',
  ]
]); ?>

<div class="nk-main">
  <!-- Header -->
  <div class="nk-header-title nk-header-title-full nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-4"><img src="/img/gallery-3.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <div class="nk-gap-2"></div>
            <h1 class="nk-title display-4">DayZ - survival </h1>
            <div class="nk-gap-2"></div>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="https://store.steampowered.com/app/221100/DayZ/" class="nk-btn nk-btn-lg">
                <span class="icon ion-steam"></span>
                <span>Buy on Steam</span>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?= $gameList['DayZ']['server']['link'] ?? '#'; ?>" target="_blank" class="nk-btn nk-btn-lg">
                <span class="icon ion-steam"></span>
                <span>Join server</span>
              </a>
              <div class="nk-gap-2"></div>
              <div class="row">
                <div class="col-md-8 offset-md-2">
                  <div class="nk-plain-video" data-video="https://www.youtube.com/watch?v=7jk8prJA9nI" data-video-thumb="/img/dayz-1.jpg"></div>
                </div>
              </div>
              <div class="nk-gap-4"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="nk-box bg-dark-1" id="1info">
      <div class="container text-center">
        <div class="nk-gap-6"></div>
        <div class="nk-gap-2"></div>
        <h2 class="nk-title h1">Arkadia PvEvP</h2>
        <div class="nk-gap-3"></div>
        <p class="lead">
          No man could have possibly imagined that a land like in dayz would ultimately sign the extincion of all civilization.
          The world would appear blindedfolded about work of rebuilding a broken country would eventually perish.
          The peace that was achieved was going to be buried.
          Nobody thought something like this could happen, because that's only movies and TV shows.
          And the likelyhood that something catastrophic did occur most people trusted the authorities and government to do the right thing and protect their beloved citizens.
        </p>
        <p class="lead">
          The connectivity of a modern society had led the human race like lambs into a forest full of wolves.
          We assumed the best and did not pay attention to what was actually happening.
          Ignorant, self-absorbed people who knew everything and yet understood nothing.
          Those observant individuals that saw it coming were too few and were disregarded, branded as conspiracy theorists and nerds.
          What they saw coming was not the plague only, but how humanityâ€™s illogical handling of a logical crisis would ultimately doom it to near-extinction.
          Global chaos that had been seen in the past will not compare to what will be seen in the coming daysâ€¦ or after those days had passed.
        </p>
        <div class="nk-gap-2"></div>
        <p class="lead">
          No-one really talks about what the goverment is hiding from us right?
          I mean, they always keep these secrets.
          Why didn't we the civillians get a warning, but the goverment personel got a heads up for that this shit-show.
          Aw fuck it, I mean we got way too many problems these days like...
          These felsh eating folk trying to end us and there's bandits and just to top it all off we have bears and wolves beacause why not?
          Alright alright let's be formal here shall we?
          Welcome to Livonia the land of many beautiful forest and wildlife.
          We have one issue you know...
          ehmm we call them infected.
          You see if you see something humanoid and just starts to scream at you and it is agressive, well that's them.
          Best option is to avoid them at all cost, but if you got a sweet Rifle and you think you can take them down.
          Go for it!
          One less infected is better then nothing.
          But keep these rifles are handy, if you can survive long enough to find one.
          Oh, and one thing before you turn your radio off...
          You, are not alone.
        </p>
        <div class="nk-gap-2"></div>
        <div class="nk-gap-6"></div>
        </div>
      </div>

      <!-- Features -->
      <div class="nk-box text-center text-white bg-dark-1" id="2info">
        <div class="bg-image bg-image-parallax op-5"><img src="/img/games/dayz/img1.jpg" alt="" class="jarallax-img"></div>
        <div class="nk-gap-4"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-md-left">
              <h2 class="nk-title">Base Building</h2>
              <h3 class="nk-sub-title text-white">The best Base Building system there is.</h3>
              <h3 class="nk-sub-title text-white">No raiding tools, your own safe heaven.</h3>
              <h3 class="nk-sub-title text-white">Build the way you like it!</h3>
            </div>
          </div>
        </div>
        <div class="nk-gap-4"></div>
      </div>
      <div class="nk-box text-center text-white bg-dark-1">
        <div class="bg-image bg-image-parallax op-5"><img src="/img/games/dayz/img2.jpg" alt="" class="jarallax-img"></div>
        <div class="nk-gap-4"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-md-left">
              <h2 class="nk-title ">Trader</h2>
              <h3 class="nk-sub-title text-white">The best economical system there is.</h3>
              <h2 class="nk-sub-title text-white">Buy and sell weapons, gear, food, base building parts, car parts and more!</h2>
              <h3 class="nk-sub-title text-white">Make sure you have the money to keep going.</h3>
            </div>
          </div>
        </div>
        <div class="nk-gap-4"></div>
      </div>
      <div class="nk-box text-center text-white bg-dark-1" >
        <div class="bg-image bg-image-parallax op-5"><img src="/img/games/dayz/img3.png" alt="" class="jarallax-img"></div>
        <div class="nk-gap-4"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-md-left">
              <h2 class="nk-title ">Modded cars</h2>
              <h3 class="nk-sub-title text-white">Wicked cars that you can find on the roads</h3>
              <h3 class="nk-sub-title text-white">For every person is a car that they would like.</h3>
              <h3 class="nk-sub-title text-white">over 10+ custom cars to be found. </h3>
            </div>
          </div>
        </div>
        <div class="nk-gap-4"></div>
      </div>
      <div class="nk-box text-center text-white bg-dark-1">
        <div class="bg-image bg-image-parallax op-5"><img src="/img/games/dayz/img4.jpg" alt="" class="jarallax-img"></div>
        <div class="nk-gap-4"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-md-left">
              <h2 class="nk-title">Loads of extra's</h2>
              <h3 class="nk-sub-title text-white">over 20+ of weapons</h3>
              <h3 class="nk-sub-title text-white">over 75+ of Foods</h3>
              <h3 class="nk-sub-title text-white">over 30+ of gear</h3>
              <h3 class="nk-sub-title text-white">over 20+ of Barrels</h3>
              <h3 class="nk-sub-title text-white">And a lot more!</h3>
            </div>
          </div>
        </div>
        <div class="nk-gap-4"></div>
      </div>
      <div class="nk-box bg-dark-1" id="info">
        <div class="nk-gap-4"></div>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-8 offset-lg-2">
              <div class="nk-tabs">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">About the game</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">Our Gameserver</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab">Our server rules</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                    <div class="nk-gap-3"></div>
                    <div class="row vertical-gap">
                      <div class="row">
                        <div class="col-md-9 offset-md-2">
                          <h2>DayZ - About</h2>
                          <p>
                            How long can you survive a post-apocalyptic world?
                            A land overrun with an infected "zombie" population, where you compete with other survivors for limited resources.
                            Will you team up with strangers and stay strong together?
                            Or play as a lone wolf to avoid betrayal?
                            This is DayZ â€“ this is your story.
                          </p>
                          <p>
                            Build up a base, set up camp and protect yourself of incoming zombies.
                            Work yourself up to the top or die trying in this wicked survival game.
                            There is blood everywhere.
                            The infected are a high risk for human society. 
                            The people that left the lands made a huge mess.
                            The world is to an end and there is no one that can stop that.
                            but, who knows you might something valuable scattered around the world of DayZ.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="nk-gap-1"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                    <div class="nk-gap-3"></div>
                    <div class="row vertical-gap">
                      <div class="row">
                        <div class="col-md-9 offset-md-2">
                          <h3>Our Gameserver</h3>
                          <p>
                            Our DayZ server is running with several plugins that changes from time to time to provide a wide viarity of gameplay for everyone.
                            DayZ is the top number 1 game when it comes on hardcore survival where you even get sick.
                          </p>
                          <p>
                            With more weapons, more food, more gear and even more custom cars that you normally wont find in the game are added to provide you a wicked gamesession.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="nk-gap-1"></div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                    <div class="nk-gap-3"></div>
                    <div class="row vertical-gap">
                      <div class="row">
                        <div class="col-md-9 offset-md-2">
                          <h2>Game server rules</h2>
                            <p>
                              The dayz server is running with a few server rules.
                              Without these rules, we cant provide support and without support, what does a admin else have to do.
                              These rules provided for the server gives everyone a fair advantage when it comes on playing on our server.
                            </p>
                            <p></p>
                            <p><strong>Server Rules</strong></p>
                            <p>
                              <ol>
                                <li>It is forbidden to use any hacks/macro/scripts on our gameserver. </li>
                                <li>We respect eachother no matter what.</li>
                                <li>Trader/save zone Camping/basebuilding is forbidden.</li>
                                <li>Only PvP is allowed inside the right zones. These zones are indicated on the map.</li>
                                <li>No advertising with websites or twitch/youtube links in any way.</li>
                              </ol>
                            </p>
                            <p>More additional Game Server Rules can be found on our discord.</p>
                          </div>
                        </div>
                      </div>
                      <div class="nk-gap-1"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="nk-gap-3"></div>
        </div>

        <!-- Video Block -->
        <div class="nk-box bg-dark-1 text-white " id="video">
          <div class="nk-video-fullscreen-toggle-right">
            <a class="nk-video-fullscreen-toggle" href="https://www.youtube.com/watch?v=7jk8prJA9nI">
              <span class="nk-video-icon"><i class="fa fa-play pl-5"></i></span>
            </a>
          </div>
          <div class="bg-video-row row justify-content-end">
            <div class="col-lg-6">
              <div class="bg-video bg-video-parallax" style="background-image: url('/img/gallery-3.jpg');"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-5">
                <div class="nk-gap-5"></div>
                <h2 class="h1 nk-title">DayZ - Trailer</h2>
                <h3 class="nk-sub-title">Check out the trailer!</h3>
                <div class="nk-gap"></div>
                <h2>DayZ - About our gameserver</h2>
                <p>
                  The only aim in DayZ is to survive.
                  To do this you will need to overcome struggles such as hunger, thirst, cold and avoid getting sick.
                  Build a fire.
                  Build a shelter.
                  Kill animals for meat or search for leftovers in lost and forgotten cities.
                  Protect yourself from other hostiles.
                  Create alliances with other players and form a nice town.
                  Be the hero that everyone is waiting for a go in battle against other clans to make peace once more.
                </p>
                <p></p>
                <div class="nk-gap-5"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>