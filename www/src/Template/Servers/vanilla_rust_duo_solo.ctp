<?= $this->element('layout/game-menu', [
  'items' => [
    'Server Information' => '#1info',
    'Why Radsz?' => '#2info',
    'Rules' => '#serverrules',
  ]
]); ?>


<div class="nk-main">
  <div class="nk-header-title nk-header-title-full nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-4"><img src="/img/games/rust/img6.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <div class="nk-gap-2"></div>
            <h1 class="nk-title display-4">Radsz - Vanilla + Duo/Solo </h1>
            <div class="nk-gap-2"></div>
            </a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="https://store.steampowered.com/app/252490/Rust/" class="nk-btn nk-btn-lg">
              <span class="icon ion-steam"></span>
              <span>Steam</span>
            </a>
            </a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?= $gameList['Vanilla Rust Duo/Solo']['server']['link'] ?? '#'; ?>" class="nk-btn nk-btn-lg">
              <span class="icon ion-steam"></span>
              <span>Join the server</span>
            </a>
            <div class="nk-gap-2"></div>
            <div class="nk-gap-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="nk-box bg-dark-1" id="1info">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Radsz - The Vanilla server</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        The world of Radsz awaits you.
        While you wake up slowly, you find out that you got dumped on a island with nothing more then a simple rock and a torch.
        The last thing you remember is the strange interview you had with the cobalt test facility.
        While you walk the world of Radsz, you find out that you are not alone.
        Wildlife, loyal ex scientists and other humans just like you dumped in the middle of nowhere.
      </p>
      <p class="lead">
        While you are trying to search for answers, the world works against you.
        Hostiles, wildlife and other players are trying to do what it takes to survive.
        Are you ready to enter the world of radsz and are you going to be the next hero that everyone is waiting for?
      </p>
      <div class="nk-gap-6"></div>
    </div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1" id="2info">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img1.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title">Prep the base and start to survive</h2>
          <h3 class="nk-sub-title text-white">Start gathering your Resources and build up your base.</h3>
          <h3 class="nk-sub-title text-white">Gather food, hunt and scavange for materials to stay alive.</h3>
          <h3 class="nk-sub-title text-white">Watch out for wildlife, Patrolling AI or even worse, other hostile players.</h3>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img6.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Community Game Rules</h2>
          <h3 class="nk-sub-title text-white">After every wipe, everyone gets a 48H Grace period. Meaning that Raiding is not allowed 2 days after a wipe.</h3>
          <h2 class="nk-sub-title text-white">Max of 3 players each team.</h2>
          <h2 class="nk-sub-title text-white">Very Low upkeep, Making a nice base for yourself is possible.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5"><img src="/img/games/rust/img3.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Including</h2>
          <h3 class="nk-sub-title text-white">A wicked alert from the server when a patrol helicopter comes in. </h3>
          <h2 class="nk-sub-title text-white">24/7 monitored by rustadmin.</h2>
          <h2 class="nk-sub-title text-white">Lots and lots of funtime with friends and clanmembers.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5">
      <img src="/img/games/rust/img4.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Monthly Wipes</h2>
          <h3 class="nk-sub-title text-white">Every first Thursday of the month. </h3>
          <h2 class="nk-sub-title text-white">With no Blueprint wipes.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box bg-dark-1" id="serverrules">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Gameserver rules</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        These rules you see here will be on most community servers.
        However, to keep everything clean and tidy, we have some rules setup in order to give you a good example on what's posible and what's not possible on our game server.
        By joining our server, you accept that</p>
      <div class="nk-gap-2"></div>
      <ol>
        <li>You dont advertise on our server for your own benefits. skin gamble sites are a instant perm ban.</li>
        <li>Dont be a troll in our gameserver. Respect others their mentions.</li>
        <li>No hate speeches, political slures, racism or sexual chat sessions.</li>
        <li>No hate speeches, political slures, racism or sexual images.</li>
        <li>Raiding is part of the game. If you dont like it, please join our PvEvP server where raiding is limited.</li>
        <li>The useage of any macro scripting, hacking or anything that needs a 3th party. Hacking is forbidden. Simple.</li>
        <li>Harrassing partners and or the team with unneeded questions and trashtalk is forbidden.</li>
        <li>Its forbidden to streamsnipe. People doing this will be banned.</li>
        <li>Building off ( external walls ) a cave is allowed. Same as for Quarries.</li>
        <li>There is no need to raid the admin base/store if there is one. Nothing will be stored in it, only our bodies remains with crappy loot.</li>
        <li>There is no such thing as admin abuse. Players clutting the chat with this nonsense will be removed.</li>
      </ol>
      <div class="nk-gap-2"></div>
      <p>
        <a href="<?= $gameList['Vanilla Rust']['server']['link'] ?? '#'; ?>" target="_blank" class="nk-btn nk-btn-lg nk-btn-color-main-1 link-effect-4">
          <span class="icon ion-steam"></span>
          <span>Join the server</span>
        </a>
      </p>
      <div class="nk-gap-6"></div>
    </div>
  </div>
</div>
