<?= $this->element('layout/game-menu', [
  'items' => [
    'Server Information' => '#1info',
    'Why Safehold?' => '#2info',
    'Media' => '#dsvideo',
  ]
]); ?>

<div class="nk-main">
  <div class="nk-header-title nk-header-title-full nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-4">
      <img src="/img/image-3-ds.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <div class="nk-gap-2"></div>
            <div class="nk-info-box bg-danger">
              <div class="nk-info-box-icon">
                <i class="ion-alert-circled"></i>
              </div>
              You'll need to manually search for our server in the serverlist of the game. Search for "SafeHold" in the searching section.
            </div>
            <div class="nk-gap-1"></div>
            <h1 class="nk-title display-4">Deadside</h1>
            <div class="nk-gap-2"></div>
            </a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="https://store.steampowered.com/app/895400/Deadside/" class="nk-btn nk-btn-lg">
              <span class="icon ion-steam"></span>
              <span>Steam</span>
            </a>
            <div class="nk-gap-2"></div>
            <div class="row">
              <div class="col-md-8 offset-md-2">
                <div class="nk-plain-video" data-video="https://www.youtube.com/watch?v=W_Oaujmj9T0" data-video-thumb="/img/video-1-thumb-ds1.jpg"></div>
              </div>
            </div>
            <div class="nk-gap-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="nk-box bg-dark-1" id="1info">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Safehold - Deadside community server</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        Deadside is a hardcore multiplayer shooter with large open world and wide gameplay possibilities.
        Deadside gets you into realistic game environment full of hopelessness and despair.
        It tries to keep balance between the dynamics of the shooter and the hardcoreness of “survival” gameplay.
        The world of the game does not imply the existence of fantastic elements like zombies or anomalies;
        Concentrating the player's attention on realistic aspects of life on the ruins of the dead civilization.
      </p>
      <p class="lead">As the world looks forgotten, it still contains its loyal soldiers. Are you brave enough to join this awesome PvEvP server?</p>
      <div class="nk-gap-6"></div>
    </div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1" id="2info">
    <div class="bg-image bg-image-parallax op-5">
      <img src="/img/games/deadside/img2.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title">Build a base</h2>
          <h3 class="nk-sub-title text-white">Build up your base in the middle of the woods, waterside or near roads.</h3>
          <h3 class="nk-sub-title text-white">Store your found goodies and keep them safe from others.</h3>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5">
      <img src="/img/games/deadside/img3.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Ingame missions and events</h2>
          <h3 class="nk-sub-title text-white">With over 25+ ingame missions and counting.</h3>
          <h2 class="nk-sub-title text-white">Every weekend with awesome events!</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5">
      <img src="/img/games/deadside/img4.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-md-left">
          <h2 class="nk-title ">Wipes with every new gameupdate.</h2>
          <h3 class="nk-sub-title text-white">With every new update, a fresh start will be happening as well. </h3>
          <h2 class="nk-sub-title text-white">Giving you the ability to build a new base elsewhere.</h2>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
  </div>
  <div class="nk-box bg-dark-1 text-white " id="dsvideo">
    <div class="bg-video-row row justify-content-end">
      <div class="col-lg-6">
        <div class="bg-video bg-video-parallax" style="background-image: url('/img/games/deadside/img6.jpg');"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="nk-gap-5"></div>
          <h2 class="h1 nk-title">Deadside - Trailer</h2>
          <h3 class="nk-sub-title">Check out the trailer!</h3>
          <div class="nk-gap"></div>
          <p>
            The world of Deadside is vast open spaces full of bushy forests and abandoned settlements.
            Players will have an area of ​​about 225 sq.km., entangled by a network of roads and rivers.
            In addition to players, the game world is inhabited by various hostile and friendly NPCs.
            Going through the forest, you can face a gang of cannibals just about to eat lunch.
            Or a group of wanderers trying to survive in a hostile environment.
            The results of these meetings are completely dependent on your decisions and ability to get those decisions to life. Or death.
          </p>
          <p>Our Gameserver is user friendly and it doenst contain any toxicity or party breakers. </p>
          <p>Game server rules</p>
          <ol>
            <li>It is forbidden to use any hacks/macro/scripts on our gameserver.</li>
            <li>We respect eachother.</li>
            <li>Trader/save zone Camping/basebuilding is forbidden.</li>
            <li>You gently keep an eye on the /info command as it contains valuable information.</li>
            <li>Play our map at own risk. It might have its minor issues. Not everyone is 100% perfect in mapping.</li>
            <li>No advertising with websites or twitch/youtube links in any way.</li>
            <li>Its forbidden to stream snipe. People doing this will be banned.</li>
          </ol>
          <p>What is your choise going to be? Join our community servers today!</p>
          <div class="nk-gap-5"></div>
        </div>
      </div>
    </div>
  </div>
</div>