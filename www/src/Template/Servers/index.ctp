<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Games', 
    'subheader' => 'Awesome games we play',
    'background' => '/img/rdmimg/skull-and-bones.jpg',
  ]); ?>

  <div class="nk-gap-4"></div>

  <!-- Game list -->
  <div class="container">
    <div class="row vertical-gap lg-gap">
      <?php foreach($gameList as $title => $options): ?>
        <?php if(empty($options['box'])) continue; ?>
        <?= $this->element('game-box', ['title' => $title, 'options' => $options['box'], 'button' => 'Read More']); ?>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="nk-gap-4"></div>
  <div class="nk-gap-4"></div>
</div>