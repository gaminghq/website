<div class="nk-share-buttons nk-share-buttons-<?= $align ?? 'left'; ?> d-none d-md-flex">
  <ul>
    <li>
      <span class="nk-share-icon" title="Share page on Facebook" data-share="facebook">
        <span class="icon fa fa-facebook"></span>
      </span>
      <span class="nk-share-name">Facebook</span>
    </li>
    <li>
      <span class="nk-share-icon" title="Share page on Twitter" data-share="twitter">
        <span class="icon fa fa-twitter"></span>
      </span>
      <span class="nk-share-name">Twitter</span>
    </li>
  </ul>
</div>