<footer class="nk-footer nk-footer-parallax nk-footer-parallax-opacity">
  <img class="nk-footer-top-corner" src="/img/footer-corner.png" alt="">
  <div class="container">
    <div class="nk-gap-2"></div>
    <div class="nk-gap"></div>
    <p>&copy; 2020 GamingHQ. Developed in association with <a href="https://www.finlaydag33k.nl" target="_blank">FinlayDaG33k</a>. All other trademarks or trade names are the property of their respective owners. All Rights Reserved. </p>
    <p>GamingHQ &reg;: GamingHQ is the online platform for gamers that have their own community, clan, team or playing as a lonewolf. GamingHQ brings you together and provides secure gameservers to be played on without the unneeded hackers, scammers or other rotten apples. GamingHQ is providing a hacker free zone and enjoyable content at the same time. </p>
    <div class="nk-footer-links">
      <a href="#" class="link-effect">Terms of Service</a> <span>|</span> <?= $this->Html->link('Privacy', ['controller' => 'Pages', 'action' => 'display', 'privacy'], ['class' => 'link-effect']); ?><span>|</span> <?= $this->Html->link('Ban Appeals', ['controller' => 'Pages', 'action' => 'display', 'ban-appeal'], ['class' => 'link-effect']); ?>
    </div>
    <div class="nk-gap-4"></div>
    <!-- Advertisement -->
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle" style="display:block;" data-ad-format="auto" data-full-width-responsive="true" data-ad-client="ca-pub-8260694614304851" data-ad-slot="4447788079"></ins>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
  </div>
</footer>