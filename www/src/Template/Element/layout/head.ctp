<!-- Meta -->
<?= $this->Html->charset(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="GamingHQ">
<meta property="og:description" content="">
<meta property="og:site_name" content="GamingHQ">
<meta property="og:type" content="">
<meta property="og:url" content="<?= $this->Url->build(null, []); ?> ">
<title>GamingHQ Website</title>

<!-- Prefetch DNS -->
<link rel="dns-prefetch" href="https://fonts.googleapis.com/">
<link rel="dns-prefetch" href="https://fonts.gstatic.com/">
<link rel="dns-prefetch" href="https://stackpath.bootstrapcdn.com/">
<link rel="dns-prefetch" href="https://code.jquery.com/">
<link rel="dns-prefetch" href="https://cdn.jsdelivr.net/">
<link rel="dns-prefetch" href="https://ka-f.fontawesome.com/">
<link rel="dns-prefetch" href="https://unpkg.com/">
<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com/">

<!-- Preconnect to CDNs -->
<link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="https://stackpath.bootstrapcdn.com/" crossorigin>
<link rel="preconnect" href="https://code.jquery.com/" crossorigin>
<link rel="preconnect" href="https://cdn.jsdelivr.net/" crossorigin>
<link rel="preconnect" href="https://ka-f.fontawesome.com/" crossorigin>
<link rel="preconnect" href="https://unpkg.com/" crossorigin>
<link rel="preconnect" href="https://cdnjs.cloudflare.com/" crossorigin>

<!-- Prefetch Stylesheets -->
<link rel="prefetch" as="style" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,400i,700%7cMarcellus+SC">
<link rel="prefetch" as="style" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="prefetch" as="style" href="https://ka-f.fontawesome.com/releases/v5.15.1/css/free-v4-font-face.min.css">
<link rel="prefetch" as="style" href="https://ka-f.fontawesome.com/releases/v5.15.1/css/free-v4-shims.min.css">
<link rel="prefetch" as="style" href="/css/godlike.css">

<!-- Prefetch Scripts -->
<link rel="prefetch" as="script" href="https://code.jquery.com/jquery-3.4.1.slim.min.js">
<link rel="prefetch" as="script" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
<link rel="prefetch" as="script" href="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js">

<!-- Prefetch Fonts -->
<link rel="prefetch" as="font" href="https://fonts.gstatic.com/s/robotocondensed/v19/ieVl2ZhZI2eCN5jzbjEETS9weq8-19K7DQ.woff2" crossorigin>

<!-- Preload Images -->
<link rel="preload" href="/img/gaminghq logo.png" as="image">
<link rel="preload" href="/img/border-top.png" as="image">
<link rel="preload" href="/img/border-bottom.png" as="image">
<link rel="preload" href="/img/border-left.png" as="image">
<link rel="preload" href="/img/border-right.png" as="image">

<!-- Consume Stylesheets -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,400i,700%7cMarcellus+SC" defer>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" defer>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous" defer>
<link rel="stylesheet" href="https://ka-f.fontawesome.com/releases/v5.15.1/css/free-v4-font-face.min.css" defer>
<link rel="stylesheet" href="https://ka-f.fontawesome.com/releases/v5.15.1/css/free-v4-shims.min.css" defer>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" defer>
<link rel="stylesheet" href="/css/vendor/revolution/settings.css" defer>
<link rel="stylesheet" href="/css/vendor/revolution/layers.css" defer>
<link rel="stylesheet" href="/css/vendor/revolution/navigation.css" defer>
<link rel="stylesheet" href="/css/vendor/ionicons/ionicons.css" defer>
<link rel="stylesheet" href="/css/godlike.css" defer>

<!-- Consume Scripts -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous" defer></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous" defer></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous" defer></script>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/object-fit-images@3.2.4/dist/ofi.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" integrity="sha512-IQLehpLoVS4fNzl7IfH8Iowfm5+RiMGtHykgZJl9AWMgqx0AmJ6cRWcB+GaGVtIsnC4voMfm8f2vwtY+6oPjpQ==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollToPlugin.min.js" integrity="sha512-nTHzMQK7lwWt8nL4KF6DhwLHluv6dVq/hNnj2PBN0xMl2KaMm1PM02csx57mmToPAodHmPsipoERRNn4pG7f+Q==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sticky-kit/1.1.3/sticky-kit.min.js" integrity="sha512-MAhdSIQcK5z9i33WN0KzveJUhM2852CJ1lJp4o60cXhQT20Y3friVRdeZ5TEWz4Pi+nvaQqnIqWJJw4HVTKg1Q==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.12.4/jarallax.min.js" integrity="sha512-XiIA4eXSY4R7seUKlpZAfPPNz4/2uzQ+ePFfimSk49Rtr/bBngfB6G/sE19ti/tf/pJ2trUbFigKXFZLedm4GQ==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.12.4/jarallax-video.min.js" integrity="sha512-+YWM6JXbsstfv0B/k8iE+xR6wEkIWyOvnB7xqzqcSp6Nkt/UWxGfEuAD3nYfaTl+uF9PsFm9dnm24+sgnL7NBQ==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.min.js" integrity="sha512-u60RZhsQhxJA7fqX4ek1kuhflszOTVeqDxEKb5ewa3KxegvBpZyeYZMm/wBZTFmVZwgt9SaqsmR0ICjQXaqH2w==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha512-Zq2BOxyhvnRFXu0+WE6ojpZLOU2jdnqbrM1hmVdGzyeCa1DgM3X5Q4A/Is9xA1IkbUeDd7755dNNI/PzSf2Pew==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js" integrity="sha512-2R4VJGamBudpzC1NTaSkusXP7QkiUYvEKhpJAxeVCqLDsgW4OqtzorZGpulE3eEA7p++U0ZYmqBwO3m+R2hRjA==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe-ui-default.min.js" integrity="sha512-SxO0cwfxj/QhgX1SgpmUr0U2l5304ezGVhc0AO2YwOQ/C8O67ynyTorMKGjVv1fJnPQgjdxRz6x70MY9r0sKtQ==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.11/typed.min.js" integrity="sha512-BdHyGtczsUoFcEma+MfXc71KJLv/cd+sUsUaYYf2mXpfG/PtBjNXsPo78+rxWjscxUYN2Qr2+DbeGGiJx81ifg==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js" integrity="sha512-lteuRD+aUENrZPTXWFRPTBcDDxIGWe5uu0apPEn+3ZKYDwDaEErIK9rvR0QzUGmUQ55KFE2RqGTVoZsKctGMVw==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.32/moment-timezone-with-data.min.js" integrity="sha512-YfIXbIiAfl/i9LO4PUETYxh72veiVE9ixWItTOx267LiYsWVAHuTO13jEwiEFAHrBtH87a47+sehqUMX3L3i2w==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js" integrity="sha512-UXumZrZNiOwnTcZSHLOfcTs0aos2MzBWHXOHOuB0J/R44QB0dwY5JgfbvljXcklVf65Gc4El6RjZ+lnwd2az2g==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.min.js" integrity="sha512-Z1AU18k/qc1YyEkZkh+pTQ2I95w76DTli3cpFe03jaYTyX+Rsmvh5ZnTmi4Zg8gBc1QGbcwgpN0o1t9b9L1B2Q==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/soundmanager2/V2.97a.20170601/script/soundmanager2-nodebug-jsmin.min.js" integrity="sha512-qUejmbDEeRyqAabrQDj4+EmfkzPqTU9OSWW/6IIhwqqAU6n93FnQeWeExiMKTnB2ipsqUVAtimLtA1ZDSZEUtA==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/keymaster/1.6.1/keymaster.min.js" integrity="sha512-BCAhaaB0/bmrMBa8RVw/Cgqg5OOuQ+kZPWfRL7RlRb/LLfQMSuxDZ48TNxmwk3dFs+R4diG6yVsXKFNELe6trw==" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.js" integrity="sha512-+cXPhsJzyjNGFm5zE+KPEX4Vr/1AbqCUuzAS8Cy5AfLEWm9+UI9OySleqLiSQOQ5Oa2UrzaeAOijhvV/M4apyQ==" crossorigin="anonymous" defer></script>
<script src="/js/vendor/revolution/js/jquery.themepunch.tools.min.js" defer></script>
<script src="/js/vendor/revolution/js/jquery.themepunch.revolution.min.js" defer></script>
<script src="/js/vendor/revolution/js/extensions/revolution.extension.video.min.js" defer></script>
<script src="/js/vendor/revolution/js/extensions/revolution.extension.carousel.min.js" defer></script>
<script src="/js/vendor/revolution/js/extensions/revolution.extension.navigation.min.js" defer></script>
<script src="/js/godlike.min.js" defer></script>
<script src="/js/godlike-init.js" defer></script>
<script src="/js/revolution-init.js" defer></script>
