<div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
  <div class="bg-image"><img src="<?= $background ?? '/img/image-1.jpg'; ?>" alt="" class="jarallax-img"></div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <h1 class="nk-title display-3"><?= h($title ?? '???'); ?></h1>
            <?php if(!empty($subheader)): ?>
              <h2 class="nk-title display-5"><?= h($subheader); ?></h2>
            <?php endif; ?>
            <div class="nk-gap-2"></div>
          <div class="nk-gap-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>