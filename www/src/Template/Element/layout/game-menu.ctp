<nav class="nk-navbar nk-navbar-side nk-navbar-left nk-navbar-lg nk-navbar-align-center nk-navbar-overlay-content" id="nk-navbar-left">
  <div class="nano">
    <div class="nano-content">
      <div class="nk-nav-table">
        <div class="nk-nav-row">
          <a href="/" class="nk-nav-logo"><img src="/img/gaminghq logo.png" alt="" width="200"></a>
        </div>
        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
          <ul class="nk-nav" data-nav-mobile="#nk-nav-mobile">
            <li class="active nk-drop-item">
              <a href="#">Home</a>
              <ul class="dropdown">
                <li><a href="/">Home</a></li>
                <li><?= $this->Html->link('All Servers', ['controller' => 'Servers', 'action' => 'index']); ?></li>
              </ul>
            </li>
            <?php foreach($items as $text => $target): ?>
              <li><a href="<?= $target; ?>"><?= h($text); ?></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>
