<div class="nk-box text-center text-white bg-dark-1">
  <div class="bg-image bg-image-parallax op-5">
    <img src="<?= $options['thumbnail'] ?? '/img/image-1.jpg'; ?>" alt="" class="jarallax-img">
  </div>
  <div class="nk-gap-5"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <h2 class="nk-title"><?= $options['name'] ?? '???'; ?></h2>
        <?php if(!empty($options['subtitle'])): ?>
          <h3 class="nk-sub-title text-main-1"><?= h($options['subtitle']); ?></h3>
        <?php endif; ?>
        <div class="nk-gap"></div>
        <p>
          <?= h($options['description']); ?>
        </p>
        <br />
        <a href="<?= $this->Url->build(['controller' => 'Servers', 'action' => 'display', 'server' => $this->Link->gameUrl($title)]); ?>" target="_blank" class="nk-btn nk-btn-x2 nk-btn-effect-2-right nk-btn-3d nk-btn-rounded">
          <span>Read more</span>
          <span class="icon"><i class="ion-ios-arrow-right"></i></span>
        </a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?= $options['link']; ?>" class="nk-btn nk-btn-x2 nk-btn-effect-2-right nk-btn-3d nk-btn-rounded nk-btn-color-main-1">
          <span>
            <?php if($this->Link->isSteam($options['link'])): ?>
              <i class="ion-steam"></i>
            <?php endif; ?>
            Join now!
          </span>
          <span class="icon"><i class="ion-ios-arrow-right"></i></span>
        </a>
      </div>
    </div>
  </div>
  <div class="nk-gap-5"></div>
  <div class="nk-gap-4"></div>
</div>
