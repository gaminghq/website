<div class="nk-counter-3">
  <div class="nk-count"><?= $amount; ?></div>
  <h3 class="nk-counter-title h4"><?= h($title); ?></h3>
  <div class="nk-gap-1"></div>
</div>