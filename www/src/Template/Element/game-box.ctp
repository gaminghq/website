<div class="col-md-6 col-lg-4">
  <div class="nk-image-box-4">
    <?= $this->Html->link('', ['controller' => 'Servers', 'action' => 'display', 'server' => $this->Link->gameUrl($title)], ['class' => 'nk-image-box-link']); ?>
    <img src="<?= $options['thumbnail'] ?? ''; ?>" alt="">
    <div class="nk-image-box-overlay nk-image-box-center">
      <div>
        <h3 class="nk-image-box-title h4 mb-20"><?= h($title ?? '???'); ?></h3>
        <div class="nk-btn"><?= h($button ?? 'Read More'); ?></div>
      </div>
    </div>
  </div>
</div>
