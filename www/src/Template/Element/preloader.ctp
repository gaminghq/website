<div class="nk-preloader">
  <div class="nk-preloader-bg" style="background-color: #000;" data-close-frames="23" data-close-speed="1.2" data-close-sprites="/img/preloader-bg.png" data-open-frames="23" data-open-speed="1.2" data-open-sprites="/img/preloader-bg-bw.png"></div>
  <div class="nk-preloader-content">
    <div>
      <img class="nk-img" src="<?= $this->Image->asString('/img/gaminghq logo.png'); ?>" alt="GamingHQ Logo" width="350">
      <div class="nk-preloader-animation"></div>
        <?php if(!empty($first_visit)): ?>
          <span>
            Getting everything ready for your first visit, hang on tight!<br />
            Next time, it should be faster!
          </span>
        <?php endif; ?>
    </div>
  </div>
  <div class="nk-preloader-skip">Skip</div>
</div>
