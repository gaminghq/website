<?= $this->element('layout/page-header', [
  'title' => 'News', 
  'subheader' => 'Fresh from the keyboard!',
  'background' => '/img/rdmimg/skull-and-bones.jpg',
]); ?>

<div class="container">
  <div class="nk-gap-4"></div>

  <div class="nk-blog-grid-3">
    <?php foreach($articles as $article): ?>
      <div class="nk-blog-post">
        <?php if(!empty($article->thumbnail)): ?>
          <div class="nk-post-thumb">
            <a href="<?= $this->Url->build(['controller' => 'Blog', 'action' => 'view', 'slug' => $article->slug]); ?>">
              <img src="<?= h($article->thumbnail); ?>" alt="" class="nk-img-stretch">
            </a>
          </div>
        <?php else: ?>
          <div class="nk-post-thumb">
            <a href="<?= $this->Url->build(['controller' => 'Blog', 'action' => 'view', 'slug' => $article->slug]); ?>">
              <img src="/img/post-7-mid.jpg" alt="" class="nk-img-stretch">
            </a>
          </div>
        <?php endif; ?>
        <div class="nk-post-content">
          <h2 class="nk-post-title h4">
            <a href="<?= $this->Url->build(['controller' => 'Blog', 'action' => 'view', 'slug' => $article->slug]); ?>">
              <?= h($article->title); ?>
            </a>
          </h2>
          <div class="nk-post-date">
            <?php if($article->created > $article->modified): ?>
              <?= $this->TimeFormat->short($article->created); ?>
            <?php else: ?>
              <?= $this->TimeFormat->short($article->modified); ?>
            <?php endif; ?>
          </div>
          <div class="nk-post-text">
            <p>
              <?= h(
                $this->PostBody->getExcerpt(
                  $this->PostBody->get($article)
                )
              ); ?>
            </p>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>

<div class="nk-gap-4"></div>
<div class="nk-gap-3"></div>