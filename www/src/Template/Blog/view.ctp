<div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity nk-header-title-boxed">
  <?php if(!empty($article->thumbnail)): ?>
    <!-- Post Thumbnail -->
    <div class="bg-image op-5">
      <img src="<?= h($article->thumbnail); ?>" alt="" class="jarallax-img">
    </div>
  <?php endif; ?>
  <div class="nk-header-table">
    <div class="nk-header-table-cell">
      <div class="container">
        <div class="nk-header-text">
          <div class="row text-left">
            <div class="col-lg-8">
              <div class="nk-gap-5 d-none d-lg-block"></div>
              <h1 class="nk-title"><?= h($article->title); ?></h1>
              <div class="nk-gap-3 d-none d-lg-block"></div>
              <div class="nk-gap-5 d-none d-lg-block"></div>
            </div>
            <!-- Post Information -->
            <div class="col-lg-4">
              <aside class="nk-sidebar nk-sidebar-right">
                <div class="nk-gap-5 d-none d-lg-block"></div>
                <div class="nk-gap d-lg-none"></div>
                <table>
                  <tbody>
                    <tr>
                      <td><strong>Published:</strong> &nbsp;&nbsp;&nbsp;</td>
                      <td>
                        <?= $this->TimeFormat->formal($article->created); ?>
                      </td>
                    </tr>
                    <?php if($article->modified > $article->created): ?>
                      <tr>
                        <td><strong>Modified:</strong> &nbsp;&nbsp;&nbsp;</td>
                        <td>
                          <?= $this->TimeFormat->formal($article->modified); ?>
                        </td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="nk-gap-5 d-lg-none"></div>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Post -->
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      <div class="nk-blog-container nk-blog-container-offset">
        <div class="nk-blog-post nk-blog-post-single">
          <div class="nk-post-text mt-0">
            <?= $this->PostBody->get($article); ?>
          </div>
        </div>
        <div class="nk-gap-4"></div>
      </div>
    </div>
  </div>
  <div class="nk-gap-4"></div>
  <div class="nk-gap-3"></div>
</div>