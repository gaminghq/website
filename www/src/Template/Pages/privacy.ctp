<div class="nk-main">
  <!-- Header -->
  <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-5">
      <img src="/img/rdmimg/thief-video-game-hd.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <h2 class="nk-title-back op-1">Privacy policy</h2>
          <h1 class="nk-title">Privacy policy</h1>
        </div>
      </div>
    </div>
    <div class="nk-header-text-bottom"> Our Privacy policy </div>
  </div>

  <!-- Overview -->
  <div class="nk-box bg-dark-1 text-white " id=" ">
    <div class="bg-video-row row justify-content-end">
      <div class="col-lg-6">
        <div class="bg-video bg-video-parallax" style="background-image: url('/img/rdmimg/skull-and-bones.jpg');"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="nk-gap-5"></div>
          <h2 class="h1 nk-title">Overview</h2>
          <div class="nk-gap"></div>
          <p>
            <h3>About this cookie policy</h3>
            <div>
              This Cookie Policy explains what cookies are and how we use them. 
              You should read this policy to understand what cookies are;
              How we use them;
              The types of cookies we use i.e, the information we collect using cookies and how that information is used and how to control the cookie preferences.
              For further information on how we use, store and keep your personal data secure, see our Privacy Policy.
              You can at any time change or withdraw your consent from the Cookie Declaration on our website.
              Learn more about who we are, how you can contact us and how we process personal data in our Privacy Policy.
              Your consent applies to the following domains:
              <a href="https://www.gaminghq.eu">www.gaminghq.eu</a>
            </div>
          </p>
          <div class="nk-gap-5"></div>
        </div>
      </div>
    </div>
  </div>

  <!-- What are Cookies? -->
  <div class="nk-box bg-dark-1 text-white " id=" ">
    <div class="bg-video-row row justify-content-end">
      <div class="col-lg-6">
        <div class="bg-video bg-video-parallax" style="background-image: url('/img/rdmimg/7445.jpg');"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
        <div class="nk-gap-5"></div>
          <div class="nk-gap"></div>
          <h3>What are cookies?</h3>
          <div>
            Cookies are small text files that are used to store small pieces of information.
            The cookies are stored on your device when the website is loaded on your browser.
            These cookies help us make the website function properly;
            Make the website more secure;
            Provide better user experience
            and understand how the website performs and to analyze what works and where it needs improvement.
          </div>
          <div class="nk-gap-2"></div>
          <h3>How do we use cookies ?</h3>
          <div>
            As most of the online services, our website uses cookies first-party and third-party cookies for a number of purposes.
            The first-party cookies are mostly necessary for the website to function the right way, and they do not collect any of your personally identifiable data.
            The third-party cookies used on our websites are used mainly for understanding how the website performs;
            How you interact with our website; keeping our services secure;
            Providing advertisements that are relevant to you;
            And all in all providing you with a better and improved user experience and help speed up your future interactions with our website.
          </div>
          <div class="nk-gap-5"></div>
        </div>
      </div>
    </div>
  </div>

  <!-- Cookies used -->
  <div class="nk-box bg-dark-1">
    <div class="container text-center">
      <div class="nk-gap-5"></div>
      <p class="lead">
        <h3>What types of cookies do we use ?</h3>
        <div>
          <ol>
            <li>
              <span style="font-weight: bold;">Essential:</span>
              Some cookies are essential for you to be able to experience the full functionality of our site.
              They allow us to maintain user sessions and prevent any security threats.
              They do not collect or store any personal information.
              For example, these cookies allow you to log-in to your account and add products to your basket and checkout securely.
            </li>
            <li>
              <span style="font-weight: bold;">Statistics:</span>
              These cookies store information like the number of visitors to the website;
              The number of unique visitors;
              Which pages of the website have been visited;
              The source of the visit etc.
              These data help us understand and analyze how well the website performs and where it needs improvement.
            </li>       
            <li>
              <span style="font-weight: bold;">Marketing:</span>
              Our website displays advertisements.
              These cookies are used to personalize the advertisements that we show to you so that they are meaningful to you.
              These cookies also help us keep track of the efficiency of these ad campaigns.
              The information stored in these cookies may also be used by the third-party ad providers to show you ads on other websites on the browser as well.
            </li>  
            <li>
              <span style="font-weight: bold;">Functional:</span>
              These are the cookies that help certain non-essential functionalities on our website.
              These functionalities include embedding content like videos or sharing contents on the website on social media platforms.
            </li>        
            <li>
              <span style="font-weight: bold;">Preferences:</span>
              These cookies help us store your settings and browsing preferences like language preferences so that you have a better and efficient experience on future visits to the website.
            </li>
          </ol>
        </div>
      </p>
    </div>
  </div>

  <!-- Cookie Preferences -->
  <div class="nk-box bg-dark-1 text-white " id=" ">
    <div class="bg-video-row row justify-content-end">
      <div class="col-lg-6">
        <div class="bg-video bg-video-parallax" style="background-image: url('/img/gallery-3.jpg');"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="nk-gap-5"></div>
          <div class="nk-gap"></div>
          <h3>How can I control the cookie preferences ?</h3>
          <div>
            Should you decide to change your preferences later through your browsing session, you can click on the "Privacy &amp; Cookie Policy" tab on your screen.
            This will display the consent notice again enabling you to change your preferences or withdraw your consent entirely.
            In addition to this, different browsers provide different methods to block and delete cookies used by websites.
            You can change the settings of your browser to block/delete the cookies.
            To find out more out more on how to manage and delete cookies, visit <a href="http://www.allaboutcookies.org/" target="_blank" rel="nofollow noopener noreferrer">www.allaboutcookies.org</a>.
          </div>
          <div class="nk-gap-5"></div>
        </div>
      </div>
    </div>
  </div>
</div>
