<div class="nk-main">
  <div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image"><img src="/img/image-1.jpg" alt="" class="jarallax-img"></div>
    <div class="nk-header-table">
      <!-- Header Title -->
      <div class="nk-header-table-cell">
        <div class="container">
          <div class="nk-header-text">
            <h1 class="nk-title display-3">GamingHQ - Multi Media Gaming </h1>
              <div class="nk-gap-2"></div>
                <?= $this->Html->link('Discord', ['controller' => 'Pages', 'action' => 'display', 'discord'], ['class' => 'nk-btn nk-btn-lg nk-btn-color-main-1 link-effect-4']); ?>
                <?= $this->Html->link('Game Servers', ['controller' => 'Pages', 'action' => 'display', 'our-servers'], ['class' => 'nk-btn nk-btn-lg link-effect-4']); ?>
                <div class="nk-gap-4"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Image slider -->
      <div class="mnt-80">
        <div id="rev_slider_50_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="photography-carousel48" style="padding:0px;">
          <div id="rev_slider_50_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.0.7">
            <ul>
              <?= $this->element('slider-image', [
                'image' => '/img/gallery-3.jpg',
                'thumbnail' => '/img/gallery-3-thumb.jpg',
                'index' => 185,
                'slide' => 'horizontal',
              ]); ?>
              <?= $this->element('slider-image', [
                'image' => '/img/gallery-5.jpg',
                'thumbnail' => '/img/gallery-5-thumb.jpg',
                'index' => 192,
                'slide' => 'vertical',
              ]); ?>
              <?= $this->element('slider-image', [
                'image' => '/img/image-3-rust-2.jpg',
                'thumbnail' => '/img/gallery-4-thumb.jpg',
                'index' => 186,
                'slide' => 'horizontal',
              ]); ?>
              <?= $this->element('slider-image', [
                'image' => '/img/gallery-1.jpg',
                'thumbnail' => '/img/gallery-1-thumb.jpg',
                'index' => 183,
                'slide' => 'horizontal',
              ]); ?>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
          </div>
        </div>
      </div>
      
      <!-- Features -->
      <div class="container">
        <div class="nk-gap-6"></div>
        <div class="nk-gap-2"></div>
        <div class="row vertical-gap lg-gap">
          <div class="col-md-4">
            <div class="nk-ibox">
              <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ios-game-controller-b"></span></div>
              <div class="nk-ibox-cont">
                <h2 class="nk-ibox-title">Incredible Game servers</h2>
                24/7 protected and updated time to time by our admins for the ultimate game experience.
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="nk-ibox">
              <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ribbon-a"></span></div>
              <div class="nk-ibox-cont">
                <h2 class="nk-ibox-title">Wicked Events</h2>
                Awesome Ingame events and gamekey giveaways from us and our partners.
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="nk-ibox">
              <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-search"></span>
            </div>
            <div class="nk-ibox-cont">
              <h2 class="nk-ibox-title">Guides</h2>
              Awesome Gaming Guides, reviews, tech reviews and more.
            </div>
          </div>
        </div>
      </div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-6"></div>
    </div>

    <!-- About -->
    <div class="nk-box bg-dark-1">
      <div class="container text-center">
        <div class="nk-gap-6"></div>
        <div class="nk-gap-2"></div>
        <h2 class="nk-title h1">About GamingHQ</h2>
        <div class="nk-gap-3"></div>
        <p class="lead">
          GamingHQ is the online platform for gamers that have their own community, clan, team or playing as a lonewolf.
          GamingHQ brings you together and provides secure gameservers to be played on without the unneeded hackers, scammers or other rotten apples.
          GamingHQ is providing a hacker free zone and enjoyable content at the same time.
        </p>
        <div class="nk-gap-2"></div>
        <div class="row no-gutters">
          <div class="col-md-4">
            <div class="nk-box-2 nk-box-line">
              <?= $this->element('counter', ['amount' => 5, 'title' => 'Communities Joined']); ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="nk-box-2 nk-box-line">
              <?= $this->element('counter', ['amount' => 5, 'title' => 'Active Game Servers']); ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="nk-box-2 nk-box-line">
              <?= $this->element('counter', ['amount' => 250, 'title' => 'Members and counting']); ?>
            </div>
          </div>
        </div>
        <div class="nk-gap-2"></div>
        <div class="nk-gap-6"></div>
      </div>
    </div>

    <!-- Video -->
    <div class="container">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="nk-plain-video" data-video="https://www.youtube.com/watch?v=wodCKgvAxeA" data-video-thumb="/img/rdmimg/video-6-thumb.png"></div>
        </div>
      </div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-6"></div>
    </div>

    <!-- Newsletter Subscription -->
    <div class="nk-box bg-dark-1">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
            <h2 class="nk-title text-center h1">Subscribe to our Newsletter</h2>
            <div class="nk-gap-3"></div>
            <!-- MailChimp Signup Form -->
            <form action="//nkdev.us11.list-manage.com/subscribe/post?u=d433160c0c43dcf8ecd52402f&amp;id=68d0448ee3" method="post" class="nk-mchimp validate" target="_blank">
              <div class="input-group">
                <input type="email" value="" name="EMAIL" class="required email form-control" placeholder="Email *">
                <button class="nk-btn nk-btn-lg link-effect-4">Subscribe</button>
              </div>
              <div class="nk-form-response-success"></div>
              <div class="nk-form-response-error"></div>
              <small>Provided by MailChimp, We'll never share your email with anyone else.</small>

              <!-- Honeypot -->
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d433160c0c43dcf8ecd52402f_68d0448ee3" tabindex="-1" value=""></div>
            </form>
          </div>
        </div>
      </div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-6"></div>
      <div class="nk-gap-4"></div>
    </div>
  </div>
</div>
