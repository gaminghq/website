<div class="nk-main">
  <!-- Header -->
  <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image op-5">
      <img src="/img/image-1.jpg" alt="" class="jarallax-img">
    </div>
    <div class="nk-header-table">
      <div class="nk-header-table-cell">
        <div class="container">
          <h2 class="nk-title-back op-1">Support</h2>
          <h1 class="nk-title">Support</h1>
        </div>
      </div>
    </div>
    <div class="nk-header-text-bottom"> NOTE: For ban appeals, please use the right forum. </div>
  </div>

  <!-- Support -->
  <div class="nk-box bg-dark-1" id="info2">
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="nk-tabs">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">Support</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">FAQ</a>
              </li>
            </ul>
            <!-- Support Tabs -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                <div class="nk-gap-3"></div>
                <div class="row vertical-gap">
                  <div class="row">
                    <div class="col-md-9 offset-md-1">
                      <h2>Support</h2>
                      <p>
                        If there is anything, we are happy to assist where we can.
                        Before you contact us, make sure you have read the FAQ first before creating a ticket.
                        Who knows it might already be awnsered.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="nk-gap-1"></div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                <div class="nk-gap-3"></div>
                <div class="row vertical-gap">
                  <div class="row">
                    <div class="col-md-9 offset-md-1">
                      <h3>Frequently asked questions</h3>
                      <ul>
                        <li>
                          <strong>Q:</strong> Are we allowed to use your rust map on our own servers?<br />
                          <strong>A:</strong> No, the useage of our map is only ment for our servers and its partners.
                        </li>                               
                        <li>
                          <strong>Q:</strong> Can I have the password for the map? I found/see something I want to see from close-by.<br />
                          <strong>A:</strong> No, noone is able to obtain the password.
                        </li>                              
                        <li>
                          <strong>Q:</strong> Why do I keep getting kicked for no reason?<br />
                          <strong>A:</strong> Your profile should be public in order to join our servers.
                        </li>                        
                        <li>
                          <strong>Q:</strong> There is a hacker on your server. What now?<br />
                          <strong>A:</strong> Report the issue on our Discord to the right staff members. They will take the required actions.
                        </li>                        
                        <li>
                          <strong>Q:</strong> Can I be a moderator/admin/CM?<br />
                          <strong>A:</strong> When we are looking for extra team members, we will be letting you know by announcement through Discord.
                        </li>
                                                        
                        <li>
                          <strong>Q:</strong> Can I re-use your content?<br />
                          <strong>A:</strong> No, taking content from our website is not allowed as the rights go the their respective owners.
                        </li>                             
                        <li>
                          <strong>Q:</strong> Do you have Discord?<br />
                          <strong>A:</strong> yes, check out the menu on top!
                        </li>
                        <li>
                          <strong>Q:</strong> I would like to share my youtube videos and twitch with you. How do I apply for this?<br />
                          <strong>A:</strong> Please send us an email towards discord@devinehq.eu
                        </li>                                                      
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="nk-gap-1"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="nk-gap-3"></div>
  </div> 
</div>