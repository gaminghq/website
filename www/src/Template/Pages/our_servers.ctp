<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Game servers',
    'subheader' => 'A peek at the servers we run',
    'thumbnail' => '/img/rdmimg/7445.jpg',
  ]); ?>

  <!-- Server list -->
  <?php foreach($gameList as $game => $options): ?>
    <?php if(empty($options['server'])) continue; ?>
    <?= $this->element('game-server', [
      'title' => $game,
      'options' => $options['server'],
    ]); ?>
  <?php endforeach; ?>

  <div class="nk-gap-5"></div>
  <div class="nk-gap-4"></div>
</div>