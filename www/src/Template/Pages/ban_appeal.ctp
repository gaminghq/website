<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Banned', 
    'subheader' => 'Ban appeal page support'
  ]); ?>

  <div class="nk-gap-4"></div>
  
  <!-- Game list -->
  <div class="container">
    <div class="row vertical-gap lg-gap">
      <?php foreach($gameList as $title => $options): ?>
        <?php if(empty($options['server'])) continue; ?>
        <?= $this->element('game-box', ['title' => $title, 'options' => $options['box'], 'button' => 'Appeal Ban']); ?>
      <?php endforeach; ?>
    </div>
  </div>

  <div class="nk-gap-4"></div>
  <div class="nk-gap-4"></div>

  <div class="nk-box bg-dark-1" id="info2">
    <div class="nk-gap-4"></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="nk-tabs">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item"><a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">Support</a></li>
              <li class="nav-item"><a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">Common ban causes</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                <div class="nk-gap-3"></div>
                <div class="row vertical-gap">
                  <div class="row">
                    <div class="col-md-9 offset-md-2">
                      <h2>Support</h2>
                      <p>
                        It may have overcome everyone.
                        Being banned while you arent aware of the thing that you did.
                        It might a breach in our TOS or who knows you got banned of some other reason.
                        From here, you will be able to apply a ban appeal.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="nk-gap-1"></div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                <div class="nk-gap-3"></div>
                <div class="row vertical-gap">
                  <div class="row">
                    <div class="col-md-9 offset-md-2">
                      <h3>Most bans are caused by</h3>
                      <p>The use of hacks, aimbots, trainers or any other 3th party program that can alter your game session.</p>
                      <p>Being a complete douch against our team.</p>
                      <p>Advertising in the chat in any way.</p>
                      <p>Trashtalking other members/partners.</p>
                      <p>Usage of racism/sexism/political slures.</p>
                      <p>VAC-banned less then 90 days ago.</p>
                      <p>Game banned less then 180 days ago.</p>
                      <p>More then 3 VAC bans means our system will perm ban you automatically.</p>
                    </div>
                  </div>
                </div>
                <div class="nk-gap-3"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="nk-gap-4"></div>
    <div class="nk-gap-4"></div>
  </div>
</div>
