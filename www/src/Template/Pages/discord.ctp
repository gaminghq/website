<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Discord',
    'subheader' => 'Disco on Discord',
  ]); ?>

  <div class="nk-box bg-dark-1">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
        <h2 class="nk-title h1">Discord Community terms of usage.</h2>
        <div class="nk-gap-3"></div>
        <p class="lead">
          To be on our discord server comes with a few rules.
          These rules you see here will be on most community servers.
          However, to keep everything clean and tidy, we have some rules setup in order to give you a good example on what's posible and what's not possible on our discord server.
          By joining our server, you accept to these terms automatically.
        </p>
        <div class="nk-gap-2"></div>
        <ol>
          <li>Advertisement of any kind if prohibited. This includes youtube/twitch linking</li>
          <li>Dont be a troll in our channels. Respect others their mentions.</li>
          <li>Dont go offtopic, dont share images in chat channels but in the right image channels.</li>
          <li>No hate speeches, political slures, racism or sexual chat sessions.</li>
          <li>Dont random join a channel if they wont expect you.</li>
          <li>Links to pirated content, torrent downloads, referal sites or gambling sites are forbidden.</li>
          <li>Harrassing partners and or the team with unneeded questions and trashtalk is forbidden.</li>
          <li>Its forbidden to streamsnipe. People doing this will be banned from the community.</li>
          <li>You share images and videos in the right proper channels. No self promotions unless you are a partner of us.</li>
          <li>Moderators may enforce rules at their own discretion; Finding loopholes to violate the rules may result to punishment.</li>
          <li>Do not beg for free Game keys or certain roles to our entire staff.</li>
          <li>You read the rules and terms of the channels you use. For example, for rust players, there are different rules then with dayz for example.</li>
        </ol>
        <div class="nk-gap-2"></div>
        <p>
          <a href="https://discord.io/GamingHQeu" target="_blank" class="nk-btn nk-btn-lg nk-btn-color-main-1 link-effect-4">
            <span>Discord</span>
          </a>
        </p>
        <div class="nk-gap-6"></div>
      </div>
    </div>
  </div>
</div>
