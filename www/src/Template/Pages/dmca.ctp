<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Multi Media Gaming',
    'subheader' => 'DMCA - Takedown request',
  ]); ?>

  <!-- Terms -->
  <div class="nk-box bg-dark-1">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">DMCA Removal Request</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        If you are a copyright owner or an agent thereof, and you believe that any content hosted on our web site (www.gaminghq.eu) infringes your copyrights, 
        then you may submit a takedown depending on the Digital Millennium Copyright Act (.DMCA.). 
        Keep in mind that we will only take actions if
        <h3>Depending on gaminghq.eu</h3>
        <ol>
 	        <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
 	        <li>Identification of the copyrighted work claimed to have been infringed, or if multiple copyrighted works on the gaminghq social website are covered by a single notification, a representative list of such works at that website.</li>
 	        <li>Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit gaminghq to locate the material. Providing URLs in the body of an email is the only way to locate those files. URLs have to be to a direct link and not to an info/search page. URLs in every DMCA notice have to be no more than 10.</li>
 	        <li>Information reasonably sufficient to permit gaminghq to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.</li>
 	        <li>A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.</li>
 	        <li>A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
 	        <li>A scanned physical document that proves you are or you are representing the copyright owner.</li>
 	        <li>A valid email address username@copyrightcompany will help you to prove you're representing or you're the legitimate copyright owner. We will not process requests from free mailboxes (Gmail, Hotmail, Yahoo, AOL, MailRu etc)</li>
 	        <li>All received emails have to be in plain/text. Our automatic system is not capable of understanding anything else.</li>
 	        <li>All mails should be sent to dmca@devinehq.eu or else they won't be processed.</li>
 	        <li>We delete only .torrent files, not search pages/info pages. Only .torrent files might or might not violate someone's copyright.</li>
          <li>Personal or attornies notices won't be processed. They have to come from companies or DMCA agents of them rightfully owner.</li>
 	        <li>Respect our work and we will respect yours. Threats won't do you any good.</li>
        </ol>

        <h3>Depending on our custom mapping/prefabs</h3>
        <ol>
 	        <li>All content you see in the map of rust is full property of FacePunch. Therefore, no DMCA can be requested.</li>
 	        <li>All our content is self-made, with items from the game itself, giving it full property rights to FacePunch once again.</li>
 	        <li>Prefabs you see ingame that are spotted elsewhere doenst mean it's stolen. We bought them or have them gifted from others or it was being offered as a free download. We will not obey your request when it comes to ingame prefabs as the items, prefabs and game items are property to FacePunch.</li>
 	        <li>A valid email address username@copyrightcompany will help you to prove you're representing or you're the legitimate copyright owner. We will not process requests from free mailboxes (Gmail, Hotmail, Yahoo, AOL, MailRu etc)</li>
 	        <li>Respect our work and we will respect yours. Threats won't do you any good.</li>
        </ol>
        &nbsp;
        Please note that under Section 512(f) of the DMCA, any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability. If you are writing from Europe please send us a European Intellectual Property Rights Enforcement Directive request.
      </p>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-6"></div>
    </div>
  </div>
</div>
