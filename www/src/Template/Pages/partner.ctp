<div class="nk-main">
  <!-- Header -->
  <?= $this->element('layout/page-header', [
    'title' => 'GamingHQ - Become a Partner',
    'subheader' => 'Let\'s unite our forces!',
    'thumbnail' => '/img/image-1.jpg',
  ]); ?>

  <div class="nk-box bg-dark-1">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">About GamingHQ</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        GamingHQ is the online platform for gamers that have their own community, clan, team or playing as a lonewolf.
        GamingHQ brings players together to enjoy the game.
        GamingHQ provides Game previews, Game Translations, awesome video and news related content about games to keep their members up to date on the ongoing progress of the gaming world.
      </p>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-3"></div>
      <p class="lead">
        Together with you as a partner, we bring your content towards our members.
        This could be anything.
        From Gamereviews, till the latest information about next patches or videos and livestream promotions trough our discord and social media.
        We have a wide open range of fans and they all have their own taste in game content.
      </p>
      <div class="nk-gap-2"></div>
      <div class="row no-gutters">
        <div class="col-md-4">
          <div class="nk-box-2 nk-box-line">
            <?= $this->element('counter', ['amount' => 5, 'title' => 'Communities Joined']); ?>
          </div>
        </div>
        <div class="col-md-4">
          <div class="nk-box-2 nk-box-line">
            <?= $this->element('counter', ['amount' => 5, 'title' => 'Active Game Servers']); ?>
          </div>
        </div>
        <div class="col-md-4">
          <div class="nk-box-2 nk-box-line">
            <?= $this->element('counter', ['amount' => 250, 'title' => 'Members and counting']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Features -->
  <div class="container">
    <div class="nk-gap-6"></div>
    <div class="nk-gap-2"></div>
    <div class="row vertical-gap lg-gap">
      <div class="col-md-4">
        <div class="nk-ibox">
          <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ios-game-controller-b"></span></div>
          <div class="nk-ibox-cont">
            <h2 class="nk-ibox-title">Incredible Game servers</h2>
            24/7 protected and updated time to time by our admins for the ultimate game experience.
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="nk-ibox">
          <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ribbon-a"></span></div>
          <div class="nk-ibox-cont">
            <h2 class="nk-ibox-title">Wicked Events</h2>
            Awesome Ingame events and gamekey giveaways from us and our partners.
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="nk-ibox">
          <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-search"></span></div>
          <div class="nk-ibox-cont">
            <h2 class="nk-ibox-title">Guides</h2>
            Awesome Gaming Guides, reviews, tech reviews and more.
          </div>
        </div>
      </div>
    </div>
    <div class="nk-gap-2"></div>
    <div class="nk-gap-6"></div>
  </div>
                        
  <!-- Becoming a partner -->
  <div class="nk-box bg-dark-1">
    <div class="container text-center">
      <div class="nk-gap-6"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Community partners</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        For those that are interested to become a partner as a game community;
        We have certain Requirements that should be checked before you apply for a partnership with the GamingHQ network.
      </p>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-3"></div>
      <div class="text-center">
        <button class="nk-btn nk-btn-x2" data-toggle="modal" data-target="#myModal">Community Requirements</button>
      </div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-2"></div>
      <h2 class="nk-title h1">Company partners</h2>
      <div class="nk-gap-3"></div>
      <p class="lead">
        For those that are interested to become a partner with us to promote your products, we would love to hear from you.
      </p>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-3"></div>
      <div class="text-center">
        <button class="nk-btn nk-btn-x2" data-toggle="modal" data-target="#myModal1">See how</button>
      </div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-2"></div>
      <div class="nk-gap-6"></div>
    </div>
  </div>
</div>

<!-- Requirements modal -->
<div id="myModal" class="modal nk-modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title nk-title" id="myModalLabel">Requirements</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="ion-android-close"></span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Being a partner with the community</h4>
        <p>To be a partner with us as a community, clan or team, you should have the following Requirements.</p>
        <ol>
          <li>You have at least 200 members in your community.</li>
          <li>You have a active social media page with at least 1000 followers/subs or a community website with own domain.</li>
          <li>You are active in at least one of our house games we play.</li>
        </ol>
        <p>
          If you dont meet any of the Requirements, we still would love to see your email.
          Emails from free email services like gmail, hotmail, outlook and such are unlikely to get a response from us.
          Please send us a email to partners@devinehq.eu
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="nk-btn nk-btn-color-main-1" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Company Requirements -->
<div id="myModal1" class="modal nk-modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title nk-title" id="myModalLabel">Requirements</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="ion-android-close"></span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Being a partner with the community</h4>
        <p>
          To be a partner with us as your company doenst have any Requirements.
          We however dont support certain companies related to their content they offer.
          These are listed down here below
        </p>
        <ol>
          <li>We dont promote any gambling content. No skin gamble sites, no casino games or any pyramide game.</li>
          <li>We dont promote any content with massive nudity in it. We are a community for players between the age of 12 and 65. We rather like to keep everything nice and tidy. Some nude is fine, but there are limits of course.</li>
          <li>We dont promote any 3th part sales websites that works like g2a for example.</li>
        </ol>
        <p>For anything else, we would love to hear from you. Together, we will be able to promote your products the right way.</p>
        <p>
          Emails from free email services like gmail, hotmail, outlook and such are unlikely to get a response from us.
          Please send us a email to partners@devinehq.eu
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="nk-btn nk-btn-color-main-1" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
