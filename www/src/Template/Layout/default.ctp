<!DOCTYPE html>
<html>
  <head>
    <?= $this->element('layout/head'); ?>
  </head>
  <body>
    <!-- Preloader -->
    <?= $this->element('preloader'); ?>

    <!-- Video background -->
    <div class="nk-page-background op-5" data-video="https://youtu.be/UkeDo1LhUqQ" data-video-loop="true" data-video-mute="true" data-video-volume="0" data-video-start-time="0" data-video-end-time="0" data-video-pause-on-page-leave="true" style="background-image: url('/img/page-background.jpg');"></div>
    <div class="nk-page-background-audio d-none" data-audio="/audio/purpleplanetmusic-desolation.mp3" data-audio-volume="100" data-audio-autoplay="false" data-audio-loop="true" data-audio-pause-on-page-leave="true"></div>
    <div class="nk-page-border">
      <div class="nk-page-border-t"></div>
      <div class="nk-page-border-r"></div>
      <div class="nk-page-border-b"></div>
      <div class="nk-page-border-l"></div>
    </div>

    <!-- Header + Navbar -->
    <header class="nk-header nk-header-opaque">
      <div class="nk-contacts-top">
        <div class="container">
          <div class="nk-contacts-left">
            <div class="nk-navbar">
              <ul class="nk-nav">
                <li class="nk-drop-item">
                  <a href="#">Social</a>
                  <ul class="dropdown">
                    <li><a href="https://www.facebook.com/devinehq">Facebook</a></li>
                    <li><a href="https://www.youtube.com/devinehq">Youtube</a></li>
                  </ul>
                </li>
                <li>
                  <?= $this->Html->link('Privacy', ['controller' => 'Pages', 'action' => 'display', 'privacy']); ?>
                </li>
                <li>
                  <?= $this->Html->link('Support', ['controller' => 'Pages', 'action' => 'display', 'support']); ?>
                </li>
                <li>
                  <?= $this->Html->link('Ban Appeals', ['controller' => 'Pages', 'action' => 'display', 'ban-appeal']); ?>
                </li>
              </ul>
            </div>
          </div>
          <div class="nk-contacts-right">
            <div class="nk-navbar">
              <ul class="nk-nav">
                <li>
                  <?= $this->Html->link('Join us on Discord', ['controller' => 'Pages', 'action' => 'display', 'discord']); ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-transparent nk-navbar-autohide">
        <div class="container">
          <div class="nk-nav-table">
            <a href="/" class="nk-nav-logo"><img src="/img/gaminghq logo.png" alt="" width="200"></a>
            <ul class="nk-nav nk-nav-right d-none d-lg-block" data-nav-mobile="#nk-nav-mobile">
              <li class="active nk-drop-item"><a href="/">Home</a></li>
              <li class="nk-drop-item">
                <?= $this->Html->link('News', ['controller' => 'Blog', 'action' => 'index']); ?>
              </li>
              <li class="nk-drop-item">
                <?= $this->Html->link('Games', ['controller' => 'Servers', 'action' => 'index']); ?>
                <ul class="dropdown">
                  <li>
                    <?= $this->Html->link('All Games', ['controller' => 'Servers', 'action' => 'index']); ?>
                  </li>
                  <li class="nk-drop-item">
                    <?= $this->Html->link('Our servers', ['controller' => 'Pages', 'action' => 'display', 'our-servers']); ?>
                    <ul class="dropdown">
                      <?php foreach($gameList as $title => $options): ?>
                        <?php if(empty($options['server'])) continue; ?>
                        <li>
                          <?= $this->Html->link($title, ['controller' => 'Servers', 'action' => 'display', 'server' => $this->Link->gameUrl($title)]); ?>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="nk-drop-item">
                <a href="https://wiki.gaminghq.eu/">Guides</a>
                <ul class="dropdown">
                  <li class="nk-drop-item">
                    <a href="https://wiki.gaminghq.eu/shelves/all-games">Gaming Guides</a>
                    <ul class="dropdown">
                      <?php foreach($gameList as $title => $options): ?>
                        <?php if(empty($options['guides']['gaming'])) continue; ?>
                        <li>
                          <a href="<?= $options['guides']['gaming']; ?>"><?= h($title); ?></a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </li>
                  <li class="nk-drop-item">
                    <a href="https://wiki.gaminghq.eu/shelves/modding">Modding Guides</a>
                    <ul class="dropdown">
                      <?php foreach($gameList as $title => $options): ?>
                        <?php if(empty($options['guides']['modding'])) continue; ?>
                        <li>
                          <a href="<?= $options['guides']['modding']; ?>"><?= h($title); ?></a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="nk-drop-item">
                <?= $this->Html->link('Get in touch', ['controller' => 'Pages', 'action' => 'display', 'support']); ?>
                <ul class="dropdown">
                  <li>
                    <?= $this->Html->link('Become a partner', ['controller' => 'Pages', 'action' => 'display', 'partner']); ?>
                  </li>
                  <li>
                    <?= $this->Html->link('DMCA', ['controller' => 'Pages', 'action' => 'display', 'dmca']); ?>
                  </li>
                </ul>
              </li>
            </ul>
            <ul class="nk-nav nk-nav-right nk-nav-icons">
              <li class="single-icon d-lg-none">
                <a href="#" class="no-link-effect" data-nav-toggle="#nk-nav-mobile">
                  <span class="nk-icon-burger">
                    <span class="nk-t-1"></span>
                    <span class="nk-t-2"></span>
                    <span class="nk-t-3"></span>
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- Mobile Nav -->
    <div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content d-lg-none">
      <div class="nano">
        <div class="nano-content">
          <a href="index.html" class="nk-nav-logo"><img src="/img/gaminghq logo.png" alt="" width="90"></a>
          <div class="nk-navbar-mobile-content">
            <ul class="nk-nav"></ul>
          </div>
        </div>
      </div>
    </div>

    <!-- Actual page content -->
    <?= $this->fetch('content'); ?>

    <!-- Back to top -->
    <div class="nk-side-buttons nk-side-buttons-visible">
      <ul>
        <li>
          <?= $this->Html->link('Support', ['controller' => 'Pages', 'action' => 'display', 'support'], ['class' => 'nk-btn nk-btn-lg link-effect-4']); ?>
        </li>
        <li class="nk-scroll-top">
          <span class="nk-btn nk-btn-lg nk-btn-icon">
            <span class="icon ion-ios-arrow-up"></span>
          </span>
        </li>
      </ul>
    </div>

    <!-- Footer -->
    <?= $this->element('layout/page-footer'); ?>

    <!-- Social Buttons -->
    <?= $this->element('layout/social-buttons', ['align' => 'left']); ?>
  </body>
</html>
