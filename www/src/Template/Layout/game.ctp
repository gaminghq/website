<!DOCTYPE html>
<html>
  <head>
    <?= $this->element('layout/head'); ?>
  </head>
  <body>
    <!-- Preloader -->
    <?= $this->element('preloader'); ?>

    <!-- Background -->
    <div class="nk-page-background op-2" data-video="" data-video-loop="true" data-video-mute="true" data-video-volume="0" data-video-start-time="0" data-video-end-time="0" data-video-pause-on-page-leave="true" style="background-image: url('/img/image-1.jpg');"></div>
    <div class="nk-page-background-audio d-none" data-audio="" data-audio-volume="100" data-audio-autoplay="true" data-audio-loop="true" data-audio-pause-on-page-leave="true"></div>
    
    <!-- Page borders -->
    <div class="nk-page-border">
      <div class="nk-page-border-t"></div>
      <div class="nk-page-border-r"></div>
      <div class="nk-page-border-b"></div>
      <div class="nk-page-border-l"></div>
    </div>

    <ul class="nk-nav-toggler-right"></ul>
    <ul class="nk-nav-toggler-left">
      <li class="d-lg-none">
        <span class="nk-btn nk-btn-lg nk-btn-icon" data-nav-toggle="#nk-nav-mobile">
          <span class="icon">
            <span class="nk-icon-burger">
              <span class="nk-t-1"></span>
              <span class="nk-t-2"></span>
              <span class="nk-t-3"></span>
            </span>
          </span>
        </span>
      </li>
    </ul>

    <div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content d-lg-none">
      <div class="nano">
        <div class="nano-content">
          <a href="/" class="nk-nav-logo"><img src="/img/gaminghq logo.png" alt="" width="90"></a>
          <div class="nk-navbar-mobile-content">
            <ul class="nk-nav"></ul>
          </div>
        </div>
      </div>
    </div>

    <!-- Actual page content -->
    <?= $this->fetch('content'); ?>

    <!-- Footer -->
    <?= $this->element('layout/page-footer'); ?>

    <!-- Social Buttons -->
    <?= $this->element('layout/social-buttons', ['align' => 'right']); ?>
  </body>
</html>