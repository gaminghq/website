#!/usr/bin/env bash
# Core CakePHP Settings
export APP_NAME="My App"
export DEBUG="true"
export APP_ENCODING="UTF-8"
export APP_DEFAULT_LOCALE="en_US"
export APP_DEFAULT_TIMEZONE="UTC"
export SECURITY_SALT="__SECURITY_SALT__"

# Database configuration
export DATABASE_URL="mysql://root:port@mysql/app?encoding=utf8&timezone=UTC&cacheMetadata=true&quoteIdentifiers=false&persistent=false"