var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rename = require("gulp-rename");
var path = require("path");
var gulpBrotli = require('gulp-brotli')
var zlib = require('zlib');

var minifyOutput = false; // set to false to disable output compression

// Set globs for this files and which not to compress
var compressFiles = [
  '/www/**/webroot/**/**/*.*',
  '!/www/**/webroot/**/**/*.br',
  '!/www/**/webroot/{scss,sass}/*',
  '!/www/**/webroot/index.php',
  '!/www/**/webroot/**/*.woff2'
];


// Create a task to compile our SASS-styles
gulp.task('compile', function() {
  // Create a watch for sass/**/*.scss (any scss file in sass/*)
  let task = gulp.src('/www/**/webroot/scss/**/*.{scss,sass}', {base : '/'});

  // Define the output style
  // Log errors
  task = task.pipe(
    sass({outputStyle: (minifyOutput == true ? 'compressed' : 'expanded')})
      .on('error', sass.logError)
  );
    
  // The output destination for our compiled *.scss file
  task = task.pipe(gulp.dest(function(file){
    let rel = path.relative(file.path, file.path.replace('/scss/','/css/'));
    let abs = path.join(path.dirname(file.path),'/webroot',rel);
    file.path = "/";
    return abs;
  }));
  
  return task;
});

// Create a task to compress all our assets with Brotli
gulp.task('compress', function() {
  return gulp.src(compressFiles, {base : '/'})
    .pipe(gulpBrotli.compress({
      extension: 'br',
      skipLarger: true,
      // the options are documented at https://nodejs.org/docs/latest-v10.x/api/zlib.html#zlib_class_brotlioptions 
      params: {
        // brotli parameters are documented at https://nodejs.org/docs/latest-v10.x/api/zlib.html#zlib_brotli_constants
        [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MAX_QUALITY,
      },
    }))
    .pipe(rename(function (path) {
      // Updates the object in-place
      path.extname = `${path.extname}.br`;
    }))
    .pipe(gulp.dest(function(file){
      return '/';
    }));
});

// Create a task to watch
gulp.task('default',function() {
  // Compile css on start
  gulp.start(['compile']);
  
  // Watch the sass/ folder for changes
  gulp.watch('/www/**/webroot/scss/**/*.{scss,sass}',['compile']);
});
